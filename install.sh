#!/bin/bash
sudo mkdir -p /root/.config/mopidy/
sudo cp mopidy.conf /root/.config/mopidy/mopidy.conf
wget -q -O - https://apt.mopidy.com/mopidy.gpg | sudo apt-key add -
sudo wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/stretch.list
sudo apt update
sudo apt install mopidy mopidy-alsamixer mopidy-spotify
sudo pip install Mopidy-Iris matplotlib
sudo systemctl enable pigpiod
sudo cp music.service /etc/systemd/system/music.service
sudo systemctl daemon-reload
sudo systemctl enable music
echo "Reboot to finish setup."
