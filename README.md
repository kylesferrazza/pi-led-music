# Pi LED Music

A python script that interacts with Mopidy to synchronize LEDs with music.

## Setup
- `git clone https://github.com/kylesferrazza/pi-led-music`
- add auth keys for [spotify][spotify-auth] to mopidy.conf
- `./install.sh`

[spotify-auth]: https://www.mopidy.com/authenticate/#spotify
