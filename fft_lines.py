#!/usr/bin/python
from numpy.fft import rfft
import numpy as np
import pigpio
import time
from matplotlib import colors as convert

RED_PIN   = 17
GREEN_PIN = 22
BLUE_PIN  = 24
CHUNKSIZE = 2048

pi = pigpio.pi()

def setLights(pin, brightness):
    pi.set_PWM_dutycycle(pin, brightness % 256)

def r(brightness):
    setLights(RED_PIN, brightness)

def g(brightness):
    setLights(GREEN_PIN, brightness)

def b(brightness):
    setLights(BLUE_PIN, brightness)

def rgb(red, green, blue):
    r(red)
    g(green)
    b(blue)

def avg(l):
    return sum(l, 0.0) / len(l)

step = 0
count = 0
red = 255
green = 0
blue = 0

def loop():
    global step
    global count
    global red
    global green
    global blue
    if step == 0:
        if green < 255:
            green += 1
        else:
            step = 1

    if step == 1:
        if blue < 255:
            blue += 1
        else:
            step = 2

    if step == 2:
        if red > 1:
            red -= 1
        else:
            step = 3

    if step == 3:
        if green > 1:
            green -= 1
        else:
            step = 4

    if step == 4:
        if red < 255:
            red += 1
        else:
            step = 5

    if step == 5:
        if blue > 1:
            blue -= 1
        else:
            step = 6

    if step == 6:
        if count > 40:
            count = 0
            step = 0
        else:
            count += 1

brightness = 0
BSTEP = 4
NO_CHANGE_DIFF = 16

stream = open('/tmp/mpd.fifo', 'rb')
while True:
    try:
        buf = np.fromstring(stream.read(CHUNKSIZE), np.int16)
        values = (np.log10(np.abs(rfft(buf)))) **2 - 5
        new_brightness = avg(values) * 30 - 300
        new_brightness = min(255, new_brightness)

        if abs(new_brightness - brightness) <= NO_CHANGE_DIFF:
            continue
        elif new_brightness > brightness and brightness < 255 - BSTEP:
            brightness += BSTEP
        elif new_brightness <= brightness and brightness > 0 + BSTEP:
            brightness -= BSTEP

        hsvs = convert.rgb_to_hsv((red, green, blue))
        hsvs[2] = brightness
        rgbs = convert.hsv_to_rgb(hsvs)

        rgb(rgbs[0], rgbs[1], rgbs[2])
        loop()
    except ValueError:
        print("transitioning")
